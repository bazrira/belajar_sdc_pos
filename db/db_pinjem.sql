-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Okt 2020 pada 04.12
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pinjem`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `kode_buku` varchar(10) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `s_active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`kode_buku`, `judul`, `s_active`) VALUES
('AN10', 'Ini Judul', '1'),
('AN101', 'Ini Judul asdas', '1'),
('AN10123', 'Ini Judulasdad', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `catatan_sewa`
--

CREATE TABLE `catatan_sewa` (
  `kode_catatan` varchar(15) NOT NULL,
  `kode_buku` varchar(10) NOT NULL,
  `kode_penyewa` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `catatan_sewa`
--

INSERT INTO `catatan_sewa` (`kode_catatan`, `kode_buku`, `kode_penyewa`) VALUES
('Catatan01', 'AN101', 'tes ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `musers`
--

CREATE TABLE `musers` (
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('admin','user') NOT NULL DEFAULT 'user',
  `s_active` enum('0','1') NOT NULL DEFAULT '1',
  `is_trash` enum('0','1') NOT NULL DEFAULT '0',
  `created_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `musers`
--

INSERT INTO `musers` (`user_id`, `email`, `name`, `password`, `level`, `s_active`, `is_trash`, `created_datetime`) VALUES
(1, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(11, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(12, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(13, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(14, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(15, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(16, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(17, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(18, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(19, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(20, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(21, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(22, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(23, 'tes@gmail.com', 'test', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37'),
(24, 'dia@gmail.com', 'ini dia', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '1', '0', '2020-09-13 12:45:37'),
(25, 'tes@gmail.com', 'apaan', 'd41d8cd98f00b204e9800998ecf8427e', 'admin', '0', '0', '2020-09-13 12:45:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyewa`
--

CREATE TABLE `penyewa` (
  `kode_penyewa` varchar(10) NOT NULL,
  `nama_penyewa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penyewa`
--

INSERT INTO `penyewa` (`kode_penyewa`, `nama_penyewa`) VALUES
('tes ', 'tes nama penyewa'),
('tes k12312', 'tes nama penyewaasdasd');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`kode_buku`);

--
-- Indeks untuk tabel `catatan_sewa`
--
ALTER TABLE `catatan_sewa`
  ADD PRIMARY KEY (`kode_catatan`),
  ADD KEY `kode_buku` (`kode_buku`),
  ADD KEY `kode_penyewa` (`kode_penyewa`);

--
-- Indeks untuk tabel `musers`
--
ALTER TABLE `musers`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeks untuk tabel `penyewa`
--
ALTER TABLE `penyewa`
  ADD PRIMARY KEY (`kode_penyewa`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `musers`
--
ALTER TABLE `musers`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `catatan_sewa`
--
ALTER TABLE `catatan_sewa`
  ADD CONSTRAINT `catatan_sewa_ibfk_1` FOREIGN KEY (`kode_penyewa`) REFERENCES `penyewa` (`kode_penyewa`),
  ADD CONSTRAINT `catatan_sewa_ibfk_2` FOREIGN KEY (`kode_buku`) REFERENCES `buku` (`kode_buku`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
