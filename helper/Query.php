<?php

class Query
{
    public function __construct()
    {
        // setup connection to database
        mysql_connect("localhost", "root", "") or die(mysql_error());
        mysql_select_db("db_pinjem");
    }

    function select($table, $column = "*", $where = null, $order_by = null, $order = "asc")
    {
        $query = "SELECT " . $column . " FROM " . $table;

        if (isset($where)) $query .= " WHERE " . $where;
        if (isset($order_by)) $query .= " ORDER BY " . $order_by . " " . $order;

        $result = mysql_query($query) or die(mysql_error());
        return $result;
    }

    function query($query)
    {
        $result = mysql_query($query) or die(mysql_error());
        return $result; // return value is 'mysql result', you can fetch it into array/assoc/row
    }

    function insert($table, $data)
    {
        $query = "INSERT INTO " . $table . "(";

        foreach ($data as $key => $value) {
            $query .= $key . ",";
        }

        $query = substr($query, 0, strlen($query) - 1) . ") VALUES (";

        foreach ($data as $value) {
            $query .= "'".$value . "',";
        }

        $query = substr($query, 0, strlen($query) - 1) . ")";
        $result = mysql_query($query) or die(mysql_error());

        return $result; // return value is boolean
    }

    function update($table, $data, $where)
    {
        $query = "UPDATE " . $table . " SET ";

        foreach ($data as $key => $value) {
            $query .= $key . "='".$value."',";
        }

        $query = substr($query, 0, strlen($query) - 1) . " WHERE ";
        $query .= $where;
        
        $result = mysql_query($query) or die(mysql_error());

        return $result; // return value is boolean
    }
}
