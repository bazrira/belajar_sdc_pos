<?php

class Route
{

    public function __constructor()
    {
    }

    public function navigation($level)
    {
        switch ($level) {
            case 'admin':
                return "template/navigation/nav_admin.php";
                break;
            case 'user':
                return "template/navigation/nav_user.php";
                break;
            default:
                return "";
                break;
        }
    }

    public function content($page, $content = null)
    {
        if ($page == 'user') {
            if ($content == 'list') {
                return "content/user/list_user.php";
            } else if ($content == 'add') {
                return "content/user/form_add_user.php";
            } else if ($content == 'edit') {
                return "content/user/form_edit_user.php";
            }
        } else if ($page == 'buku') {
            if ($content == 'list') {
                return "content/buku/list_buku.php";
            } else if ($content == 'add') {
                return "content/buku/form_add_buku.php";
            } else if ($content == 'edit') {
                return "content/buku/form_edit_buku.php";
            }
        } else if ($page == 'penyewa') {
            if ($content == 'list') {
                return "content/penyewa/list_penyewa.php";
            } else if ($content == 'add') {
                return "content/penyewa/form_add_penyewa.php";
            } else if ($content == 'edit') {
                return "content/penyewa/form_edit_penyewa.php";
            }
        } else if ($page == 'catatan') {
            if ($content == 'list') {
                return "content/catatan/list_catatan.php";
            } else if ($content == 'add') {
                return "content/catatan/form_add_catatan.php";
            } else if ($content == 'edit') {
                return "content/catatan/form_edit_catatan.php";
            }
        } else {
            return "content/dashboard/dashboard_admin.php";
        }
    }
}
