<?php

class Session
{

    public function __construct()
    {
        @session_start();
    }

    public function is_login()
    {
        if (
            @$_SESSION['is_login'] == null &&
            @$_SESSION['is_login'] == false
        ) {
            return false;
        } else if (
            @$_SESSION['is_login'] != null &&
            @$_SESSION['is_login'] == true
        ) {
            return true;
        }
    }
}
