<?php

require "helper/Session.php";

$session = new Session();
if ($session->is_login()) header('location: dashboard.php');

?>

<!DOCTYPE html>
<html>

<head>
  <title>Login</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
</head>

<body>

  <div class="container" style="margin: 0 auto;">
    <h2 class="text-center">Please sign in</h2>
    <form method="POST" action="action/action_login.php">
      <br>
      <div class="row">
        <div class="col col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 col-md-offset-3">
          <label>Email Address</label>
          <input type="email" name="email" class="form-control" placeholder="Email Address" required>
        </div>
      </div>

      <br>
      <div class="row">
        <div class="col col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 col-md-offset-3">
          <label>Password</label>
          <input type="password" name="password" class="form-control" placeholder="Password" required>
        </div>
      </div>

      <br>
      <div class="row">
        <div class="col col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 col-md-offset-3">
          <small>Belum punya Akun? <a href="register.php">Daftar disini</a></small>
          <br>
          <input type="submit" class="btn btn-primary" name="login" value="Sign In">
        </div>
      </div>
    </form>
  </div>

</body>

</html>