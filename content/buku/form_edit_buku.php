<?php

$id = @$_GET['id'];

$query = new Query();
$result = $query->select("buku", "*", "kode_buku='$id'");

$kode_buku = "";
$judul = "";
$s_active = "";

while ($data = mysql_fetch_assoc($result)) {
    $kode_buku = $data['kode_buku'];
    $judul = $data['judul'];
    $s_active = $data['s_active'];
}

?>

<div class="container">
    <div class="row">
        <div class="col col-md-8 col-md-offset-2">
            <form action="action/action_save_data_buku.php" method="POST">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="method" value="update">
                <div class="form-group">
                    <label>Kode Buku</label>
                    <input type="text" name="kode_buku" class="form-control" placeholder="Kode Buku" value="<?php echo $kode_buku; ?>" required>
                </div>
                <div class="form-group">
                    <label>Judul Buku</label>
                    <input type="text" name="judul_buku" class="form-control" placeholder="Judul Buku" value="<?php echo $judul; ?>" required>
                </div>
                <div class="form-group">
                    <label>Status Aktif</label>
                    <select class="form-control" name="s_active" required>
                        <option value="1" <?php echo ($s_active == "1") ? "selected" : "" ?>>Aktif</option>
                        <option value="0" <?php echo ($s_active == "0") ? "selected" : "" ?>>Tidak Aktif</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-disabled" onclick="history.go(-1);">Cancel</button>
            </form>
        </div>
    </div>
</div>