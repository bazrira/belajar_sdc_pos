<div class="container">
    <div class="row">
        <div class="col col-md-8 col-md-offset-2">
            <form action="action/action_save_data_buku.php" method="POST">
                <input type="hidden" name="method" value="insert">
                <div class="form-group">
                    <label>Kode Buku</label>
                    <input type="text" name="kode_buku" class="form-control" placeholder="Kode Buku" required>
                </div>
                <div class="form-group">
                    <label>Judul Buku</label>
                    <input type="text" name="judul_buku" class="form-control" placeholder="Judul Buku" required>
                </div>
                <div class="form-group">
                    <label>Status Aktif</label>
                    <select class="form-control" name="s_active" required>
                        <option value="1" selected>Aktif</option>
                        <option value="0">Tidak Aktif</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-disabled" onclick="history.go(-1);">Cancel</button>
            </form>
        </div>
    </div>
</div>