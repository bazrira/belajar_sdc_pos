<?php

$query = new Query();

$result = $query->select("buku", "*");

$body = "";
$no = 1;

if (mysql_num_rows($result) > 0) {
    while ($data = mysql_fetch_assoc($result)) {
        $body .= "<tr>";
        $body .= "<td>" . $no++ . "</td>";
        $body .= "<td>" . $data['kode_buku'] . "</td>";
        $body .= "<td>" . $data['judul'] . "</td>";
        $body .= "<td>" . strtoupper((($data['s_active'] == 0) ? "Inactive" : "Active")) . "</td>";
        $body .= "<td>";
        $body .= "<a href=\"action/action_delete_data_buku.php?id=".$data['kode_buku']."\"><button class=\"btn btn-danger\">Hapus</button></a>";
        $body .= "<a href=\"?page=buku&content=edit&id=".$data['kode_buku']."\"><button class=\"btn btn-warning\">Ubah</button></a>";
        $body .= "</td>";
        $body .= "</tr>";
    }
} else {
    $body .= "<tr class=\"text-center\"><td colspan=\"6\">Data tidak ditemukan</td></tr>";
}

?>

<div class="container">
    <a href="?page=buku&content=add"><button class="btn btn-success">Tambah Data Buku</button></a>
    <div class="table-responsive">
        <table class="table table-hover" id="table_id">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Buku</th>
                    <th>Judul</th>
                    <th>Status Aktif</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $body; ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#table_id').DataTable();
    });
</script>