<?php

$id = @$_GET['id'];

$query = new Query();
$result = $query->select("penyewa", "*", "kode_penyewa='$id'");

$name = "";
$code = "";

while ($data = mysql_fetch_assoc($result)) {
    $name = $data['nama_penyewa'];
    $code = $data['kode_penyewa'];
}

?>

<div class="container">
    <div class="row">
        <div class="col col-md-8 col-md-offset-2">
            <form action="action/action_save_data_penyewa.php" method="POST">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="method" value="update">
                <div class="form-group">
                    <label>Kode Penyewa</label>
                    <input type="text" name="code" class="form-control" placeholder="Enter code" value="<?php echo $code;?>" required>
                </div>
                <div class="form-group">
                    <label>Nama Penyewa</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter name" value="<?php echo $name;?>" required>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-disabled" onclick="history.go(-1);">Cancel</button>
            </form>
        </div>
    </div>
</div>