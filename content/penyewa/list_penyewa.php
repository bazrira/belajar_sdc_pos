<?php

$query = new Query();

$result = $query->select("penyewa", "*");

$body = "";
$no = 1;

if (mysql_num_rows($result) > 0) {
    while ($data = mysql_fetch_assoc($result)) {
        $body .= "<tr>";
        $body .= "<td>" . $no++ . "</td>";
        $body .= "<td>" . $data['kode_penyewa'] . "</td>";
        $body .= "<td>" . $data['nama_penyewa'] . "</td>";
        $body .= "<td>";
        $body .= "<a href=\"action/action_delete_data_penyewa.php?id=".$data['kode_penyewa']."\"><button class=\"btn btn-danger\">Hapus</button></a>";
        $body .= "<a href=\"?page=penyewa&content=edit&id=".$data['kode_penyewa']."\"><button class=\"btn btn-warning\">Ubah</button></a>";
        $body .= "</td>";
        $body .= "</tr>";
    }
} else {
    $body .= "<tr class=\"text-center\"><td colspan=\"6\">Data tidak ditemukan</td></tr>";
}

?>

<div class="container">
    <a href="?page=penyewa&content=add"><button class="btn btn-success">Tambah Data</button></a>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Penyewa</th>
                    <th>Nama Penyewa</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $body; ?>
            </tbody>
        </table>
    </div>
</div>