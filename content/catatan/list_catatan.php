<?php

$query = new Query();

// $result = $query->query("
// SELECT 
//     a.kode_catatan, b.judul, c.nama_penyewa 
// FROM 
//     catatan_sewa a
// JOIN 
//     buku b ON a.kode_buku = b.kode_buku
// JOIN
//     penyewa c ON a.kode_penyewa = c.kode_penyewa
// ");

$result = $query->select("catatan_sewa");

$body = "";
$no = 1;

if (mysql_num_rows($result) > 0) {
    while ($data = mysql_fetch_assoc($result)) {
        $body .= "<tr>";
        $body .= "<td>" . $no++ . "</td>";
        $body .= "<td>" . $data['kode_catatan'] . "</td>";
        $body .= "<td>" . $data['judul'] . "</td>";
        $body .= "<td>" . $data['nama_penyewa'] . "</td>";
        $body .= "<td>";
        $body .= "<a href=\"action/action_delete_data_catatan.php?id=".$data['kode_catatan']."\"><button class=\"btn btn-danger\">Hapus</button></a>";
        $body .= "<a href=\"?page=catatan&content=edit&id=".$data['kode_catatan']."\"><button class=\"btn btn-warning\">Ubah</button></a>";
        $body .= "</td>";
        $body .= "</tr>";
    }
} else {
    $body .= "<tr class=\"text-center\"><td colspan=\"6\">Data tidak ditemukan</td></tr>";
}

?>

<div class="container">
    <a href="?page=catatan&content=add"><button class="btn btn-success">Tambah Data Catatan</button></a>
    <div class="table-responsive">
        <table class="table table-hover" id="table_id">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Catatan</th>
                    <th>Judul</th>
                    <th>Nama Penyewa</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $body; ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#table_id').DataTable();
    });
</script>