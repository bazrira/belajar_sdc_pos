<?php

$id = @$_GET['id'];

$query = new Query();
$result_buku = $query->select("buku", "*");
$result_penyewa = $query->select("penyewa", "*");

$result = $query->select("catatan_sewa", "*", "kode_catatan='" . $id . "'");

$code = "";

$option_buku = "";
$option_penyewa = "";

while ($data_result = mysql_fetch_assoc($result)) {

    $code = $data_result['kode_catatan'];

    while ($data = mysql_fetch_assoc($result_buku)) {
        $option_buku .= "<option value='" . $data['kode_buku'] . "' " . (($data_result['kode_buku'] == $data['kode_buku']) ? 'selected' : '') . ">" . $data['judul'] . "</option>\n";
    }

    while ($data = mysql_fetch_assoc($result_penyewa)) {
        $option_penyewa .= "<option value='" . $data['kode_penyewa'] . "' " . (($data_result['kode_penyewa'] == $data['kode_penyewa']) ? 'selected' : '') . ">" . $data['nama_penyewa'] . "</option>\n";
    }
}

?>

<div class="container">
    <div class="row">
        <div class="col col-md-8 col-md-offset-2">
            <form action="action/action_save_data_catatan.php" method="POST">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="method" value="update">
                <div class="form-group">
                    <label>Kode Catatan</label>
                    <input type="text" name="kode_catatan" class="form-control" placeholder="Kode Catatan" value=<?php echo $code; ?> required>
                </div>
                <div class="form-group">
                    <label>Buku</label>
                    <select name="kode_buku" id="input" class="form-control" required="required">
                        <?php echo $option_buku; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Penyewa</label>
                    <select name="kode_penyewa" id="input" class="form-control" required="required">
                        <?php echo $option_penyewa; ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-disabled" onclick="history.go(-1);">Cancel</button>
            </form>
        </div>
    </div>
</div>