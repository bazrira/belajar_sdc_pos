<?php

$query = new Query();
$result_buku = $query->select("buku", "*");
$result_penyewa = $query->select("penyewa", "*");

$option_buku = "";
$option_penyewa = "";

while($data = mysql_fetch_assoc($result_buku)){
    $option_buku .= "<option value='".$data['kode_buku']."'>".$data['judul']."</option>\n";
}

while($data = mysql_fetch_assoc($result_penyewa)){
    $option_penyewa .= "<option value='".$data['kode_penyewa']."'>".$data['nama_penyewa']."</option>\n";
}

?>

<div class="container">
    <div class="row">
        <div class="col col-md-8 col-md-offset-2">
            <form action="action/action_save_data_catatan.php" method="POST">
                <input type="hidden" name="method" value="insert">
                <div class="form-group">
                    <label>Kode Catatan</label>
                    <input type="text" name="kode_catatan" class="form-control" placeholder="Kode Catatan" required>
                </div>
                <div class="form-group">
                    <label>Buku</label>
                    <select name="kode_buku" id="input" class="form-control" required="required">
                        <?php echo $option_buku;?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Penyewa</label>
                    <select name="kode_penyewa" id="input" class="form-control" required="required">
                        <?php echo $option_penyewa;?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-disabled" onclick="history.go(-1);">Cancel</button>
            </form>
        </div>
    </div>
</div>