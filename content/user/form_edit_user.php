<?php

$id = @$_GET['id'];

$query = new Query();
$result = $query->select("musers", "*", "user_id='$id'");

$name = "";
$email = "";
$level = "";
$s_active = "";

while ($data = mysql_fetch_assoc($result)) {
    $name = $data['name'];
    $email = $data['email'];
    $level = $data['level'];
    $s_active = $data['s_active'];
}

?>

<div class="container">
    <div class="row">
        <div class="col col-md-8 col-md-offset-2">
            <form action="action/action_save_data_user.php" method="POST">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="hidden" name="method" value="update">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter name" value="<?php echo $name; ?>" required>
                </div>
                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" name="email" class="form-control" placeholder="Enter email" value="<?php echo $email; ?>" required>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Kosongkan jika tidak ingin mengubah password">
                </div>
                <div class="form-group">
                    <label>Level</label>
                    <select class="form-control" name="level" required>
                        <option value="admin" <?php echo ($level == "admin") ? "selected" : "" ?>>Admin</option>
                        <option value="user" <?php echo ($level == "user") ? "selected" : "" ?>>User</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Status Aktif</label>
                    <select class="form-control" name="s_active" required>
                        <option value="1" <?php echo ($s_active == "1") ? "selected" : "" ?>>Aktif</option>
                        <option value="0" <?php echo ($s_active == "0") ? "selected" : "" ?>>Tidak Aktif</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-disabled" onclick="history.go(-1);">Cancel</button>
            </form>
        </div>
    </div>
</div>