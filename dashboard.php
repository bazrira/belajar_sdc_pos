<?php

require "helper/Route.php";
require "helper/Session.php";
require "helper/Query.php";

$session = new Session();
if (!$session->is_login()) header('location: login.php');

$route = new Route();

?>

<!DOCTYPE html>
<html>

<head>
  <title>Dashboard</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
  
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
</head>

<body>

  <div class="navigation">
    <nav class="navbar navbar-default" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <?php

      include $route->navigation(@$_SESSION['level']);

      ?>
    </nav>
  </div>

  <div class="content">
    <?php

    $page = @$_GET['page'];
    $content = @$_GET['content'];

    if (isset($page)) include $route->content($page, $content);

    ?>
  </div>



</body>

</html>