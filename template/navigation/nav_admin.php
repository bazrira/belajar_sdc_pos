<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
        <li class="active"><a href="?page=">Dashboard</a></li>
        <li><a href="?page=user&content=list">User</a></li>
        <li><a href="?page=buku&content=list">Buku</a></li>
        <li><a href="?page=penyewa&content=list">Penyewa</a></li>
        <li><a href="?page=catatan&content=list">Catatan Penyewaan</a></li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Akun<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="#">Ubah Password</a></li>
                <li><a href="action/action_logout.php">Logout</a></li>
            </ul>
        </li>
    </ul>
</div><!-- /.navbar-collapse -->