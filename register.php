<?php

require "helper/Session.php";

$session = new Session();
if ($session->is_login()) header('location: dashboard.php');

?>

<!DOCTYPE html>
<html>

<head>
  <title>Register</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

  <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
</head>

<body>

  <div class="container">
    <div class="row">
      <form method="POST" action="">
        <h2>Create New Account</h2>
        <br>
        <label>Full Name</label>
        <input type="text" name="fullname" class="form-control" placeholder="Insert Full Name" required>

        <br>
        <label>Email Address</label>
        <input type="email" name="email" class="form-control" placeholder="Email Address" required>

        <br>
        <label>Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        <small>
          <div id="textpassword">
            <a href="javascript: textshowpassword()">Tampilkan password</a>
          </div>
        </small>

        <br>
        <label>Confirmation Password</label>
        <input type="password" name="confirmation_password" class="form-control" placeholder="Confirmation Password" required>

        <br>
        <small>Sudah punya Akun? <a href="login.php">Masuk disini</a></small>

        <br>
        <input type="submit" class="btn btn-primary" name="register" value="Register">

      </form>
    </div>
  </div>
  </div>

</body>

</html>

<script>
  function textshowpassword() {
    document.getElementById('textpassword').innerHTML = '<a href="javascript: texthidepassword()">Sembunyikan password</a>';
    if (document.querySelector("[name='password']").getAttribute('type') == "password") {
      document.querySelector("[name='password']").setAttribute('type', 'text');
    }
  }

  function texthidepassword() {
    document.getElementById('textpassword').innerHTML = '<a href="javascript: textshowpassword()">Tampilkan password</a>';
    if (document.querySelector("[name='password']").getAttribute('type') == "text") {
      document.querySelector("[name='password']").setAttribute('type', 'password');
    }
  }
</script>