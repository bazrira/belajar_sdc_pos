<?php

require "../helper/Query.php";

$code = @$_POST['code'];
$name = @$_POST['name'];
$method = @$_POST['method'];

$query = new Query();

$data = array(
    "kode_penyewa" => $code,
    "nama_penyewa" => $name
);

if ($method == 'insert') {
    $data = $query->insert("penyewa", $data);
} else if ($method == 'update') {
    $data = $query->update("penyewa", $data, "kode_penyewa='" . @$_POST['id']."'");
}

if ($data) {
    echo "<script>alert('Berhasil " . $method . " data');history.go(-1);</script>";
} else {
    echo "<script>alert('Gagal " . $method . " data');history.go(-1);</script>";
}
