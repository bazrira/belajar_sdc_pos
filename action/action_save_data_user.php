<?php

require "../helper/Query.php";

$name = @$_POST['name'];
$email = @$_POST['email'];
$password = @$_POST['password'];
$level = @$_POST['level'];
$s_active = @$_POST['s_active'];
$method = @$_POST['method'];

$query = new Query();

$data = array(
    "name" => $name,
    "email" => $email,
    "password" => md5($password),
    "level" => $level,
    "s_active" => $s_active
);

if ($method == 'insert') {
    $data = $query->insert("musers", $data);
} else if ($method == 'update') {
    $data = $query->update("musers", $data, "user_id='" . @$_POST['id']."'");
}

if ($data) {
    echo "<script>alert('Berhasil " . $method . " data');history.go(-1);</script>";
} else {
    echo "<script>alert('Gagal " . $method . " data');history.go(-1);</script>";
}
