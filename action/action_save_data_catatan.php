<?php

require "../helper/Query.php";

$kode_catatan = @$_POST['kode_catatan'];
$kode_buku = @$_POST['kode_buku'];
$kode_penyewa = @$_POST['kode_penyewa'];
$method = @$_POST['method'];

$query = new Query();

$data = array(
    "kode_catatan" => $kode_catatan,
    "kode_buku" => $kode_buku,
    "kode_penyewa" => $kode_penyewa
);

if ($method == 'insert') {
    $data = $query->insert("catatan_sewa", $data);
} else if ($method == 'update') {
    $data = $query->update("catatan_sewa", $data, "kode_catatan='" . @$_POST['id']."'");
}

if ($data) {
    echo "<script>alert('Berhasil " . $method . " data');history.go(-1);</script>";
} else {
    echo "<script>alert('Gagal " . $method . " data');history.go(-1);</script>";
}
