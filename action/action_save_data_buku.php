<?php

require "../helper/Query.php";

$kode_buku = @$_POST['kode_buku'];
$judul_buku = @$_POST['judul_buku'];
$s_active = @$_POST['s_active'];
$method = @$_POST['method'];

$query = new Query();

$data = array(
    "kode_buku" => $kode_buku,
    "judul" => $judul_buku,
    "s_active" => $s_active
);

if ($method == 'insert') {
    $data = $query->insert("buku", $data);
} else if ($method == 'update') {
    $data = $query->update("buku", $data, "kode_buku='" . @$_POST['id'] . "'");
}

if ($data) {
    echo "<script>alert('Berhasil " . $method . " data');history.go(-1);</script>";
} else {
    echo "<script>alert('Gagal " . $method . " data');history.go(-1);</script>";
}
